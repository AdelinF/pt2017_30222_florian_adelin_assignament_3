package model;

/**
 * Created by Adelin on 23-Apr-17.
 */
public class Product {
    private int id;
    private String name;
    private float price;
    private int quantity;

    public int getId() {
        return id;
    }

    public Product(int id, String name, float price, int quantity){
        this.id=id;
        this.name=name;
        this.price=price;
        this.quantity=quantity;
    }

    public Product(String name, float price, int quantity){
        this.name=name;
        this.price=price;
        this.quantity=quantity;
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }
}
