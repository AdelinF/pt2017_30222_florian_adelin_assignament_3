package model;

/**
 * Created by Adelin on 23-Apr-17.
 */
public class Client {
    private int id;
    private String name;
    private String address;
    private String phone;
    private String password;

    public Client(int id, String name, String address, String phone, String password){
        this.id=id;
        this.address=address;
        this.name=name;
        this.password=password;
        this.phone= phone;
    }

    public Client(String name, String address, String phone, String password){
        this.address=address;
        this.name=name;
        this.password=password;
        this.phone= phone;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }
}
