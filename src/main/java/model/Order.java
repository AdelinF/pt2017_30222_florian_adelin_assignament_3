package model;

/**
 * Created by Adelin on 23-Apr-17.
 */
public class Order {
    private int idClient;
    private int idProduct;
    private int quantity;

    public Order(int idC, int idP, int q) {
        this.idClient = idC;
        this.idProduct = idP;
        this.quantity = q;
    }

    public int getIdClient() {
        return idClient;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public int getQuantity() {
        return quantity;
    }
}
