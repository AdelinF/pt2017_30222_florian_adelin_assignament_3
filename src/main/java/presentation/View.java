package presentation;

import bll.validators.ClientBLL;
import bll.validators.OrderBLL;
import bll.validators.ProductBLL;
import model.Client;
import model.Order;
import model.Product;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

public class View {
    LogInView log = new LogInView();
    AdminView adminView = new AdminView();
    ClientView clientView = new ClientView();
    DataInputView dataView = new DataInputView();
    private int idClient;
    public void logIn(){
        log.setVisible(true);
        log.logInBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(log.userField.getText().equals("admin")&&log.passField.getText().equals("admin")){
                    logInAsAdmin();
                }else {
                    ClientBLL clientBll = new ClientBLL();
                    Client client = clientBll.findClientById(Integer.parseInt(log.userField.getText()));
                    if (client.getPassword().equals(log.passField.getText())) {
                        idClient=client.getId();
                        logInAsClient();
                    }
                }
            }
        });
    }
    private void logInAsAdmin(){
        adminView.setVisible(true);
        adminView.newClient.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dataView.setVisible(true);
                dataView.clientOkbtn.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Client client = new Client(dataView.nameClient.getText(),dataView.addressClient.getText(),dataView.phoneClient.getText(),dataView.passClient.getText());
                        ClientBLL clientBLL = new ClientBLL();
                        clientBLL.insertClient(client);
                        dataView.setVisible(false);
                        dataView.nameClient.setText("");
                        dataView.addressClient.setText("");
                        dataView.phoneClient.setText("");
                        dataView.passClient.setText("");

                    }
                });
            }
        });
        adminView.newProduct.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dataView.setVisible(true);
                dataView.productOkbtn.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Product product = new Product(dataView.nameProduct.getText(),Float.parseFloat(dataView.priceProduct.getText()),Integer.parseInt(dataView.quantityProduct.getText()));
                        ProductBLL productBLL = new ProductBLL();
                        productBLL.insertProduct(product);
                        dataView.setVisible(false);
                    }
                });
            }
        });
        adminView.deleteClient.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dataView.setVisible(true);
                dataView.btnDeleteClient.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        ClientBLL clientBLL = new ClientBLL();
                        clientBLL.deleteClient(Integer.parseInt(dataView.deleteId.getText()));
                    }
                });
            }
        });

        adminView.deleteProduct.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dataView.setVisible(true);
                dataView.btnDeleteProduct.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        ProductBLL productBLL = new ProductBLL();
                        productBLL.deleteProduct(Integer.parseInt(dataView.deleteIdProduct.getText()));
                    }
                });
            }
        });

        adminView.listClients.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClientBLL clientBll = new ClientBLL();
                ResultSet rs = clientBll.listClients();
                try{
                    JTable table = new JTable(buildTableModel(rs));
                    JOptionPane.showMessageDialog(null, new JScrollPane(table));
                }catch (Exception ee){
                    ee.printStackTrace();
                }
            }
        });

        adminView.listProducts.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProductBLL productBll = new ProductBLL();
                ResultSet rs = productBll.listProducts();
                try{
                    JTable table = new JTable(buildTableModel(rs));
                    JOptionPane.showMessageDialog(null, new JScrollPane(table));
                }catch (Exception ee){
                    ee.printStackTrace();
                }
            }
        });

        adminView.btnListOrders.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OrderBLL orderBll = new OrderBLL();
                ResultSet rs = orderBll.listOrders();
                try{
                    JTable table = new JTable(buildTableModel(rs));
                    JOptionPane.showMessageDialog(null, new JScrollPane(table));
                }catch (Exception ee){
                    ee.printStackTrace();
                }
            }
        });

        adminView.btnMakeBill.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClientBLL clientBLL = new ClientBLL();
                Client client = clientBLL.findClientById(Integer.parseInt(adminView.billField.getText()));
                OrderBLL orderBLL = new OrderBLL();
                ResultSet orders = orderBLL.listOrders();
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date();
                System.out.println(dateFormat.format(date));
                try{
                    PrintWriter writer = new PrintWriter(client.getName()+"'s_BILL.txt", "UTF-8");
                    writer.println("Bill's date: "+date);
                    writer.println("Name: "+client.getName()+", Address: "+client.getAddress());
                    writer.println("Phone: "+client.getPhone());
                    writer.println("Products to deliver: ");
                    while(orders.next()){
                        String idClient = orders.getString("idClient");
                        String idProduct = orders.getString("idProduct");
                        int orderQuantity = orders.getInt("Quantity");
                        if(idClient.equals(""+client.getId())){
                            ProductBLL productBLL = new ProductBLL();
                            Product product = productBLL.findProductById(Integer.parseInt(idProduct));
                            writer.write(product.getName()+", Quantity: "+orderQuantity+"||"+"Price: "+orderQuantity+"X"+product.getPrice()+"="+orderQuantity*product.getPrice()+"\n");
                        }
                    }
                    writer.close();
                } catch (Exception eee) {
                    eee.printStackTrace();
                }
            }
        });

        adminView.btnBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                log.setVisible(true);
                adminView.setVisible(false);
            }
        });

    }
    private void logInAsClient(){
        clientView.setVisible(true);
        clientView.btnListProducts.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProductBLL productBll = new ProductBLL();
                ResultSet rs = productBll.listProducts();
                try{
                    JTable table = new JTable(buildTableModel(rs));
                    JOptionPane.showMessageDialog(null, new JScrollPane(table));
                }catch (Exception ee){
                    ee.printStackTrace();
                }
            }
        });
        clientView.btnOrder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OrderBLL orderBLL = new OrderBLL();
                ProductBLL productBLL = new ProductBLL();
                Product product = productBLL.findProductById(Integer.parseInt(clientView.productId.getText()));
                Order order = new Order(idClient,Integer.parseInt(clientView.productId.getText()),Integer.parseInt(clientView.quantityProduct.getText()));
                if(Integer.parseInt(clientView.quantityProduct.getText())>product.getQuantity()){
                    JOptionPane.showMessageDialog(null,"Not enough "+product.getName());
                }else {
                    orderBLL.insertOrder(order);
                    productBLL.updateProduct(product.getId(),product.getQuantity()-Integer.parseInt(clientView.quantityProduct.getText()));
                }
            }
        });
    }

    public DefaultTableModel buildTableModel(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<String>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<Vector<Object>>();
        while (rs.next()) {
            Vector<Object> vector = new Vector<Object>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }

        return new DefaultTableModel(data, columnNames);

    }
}
