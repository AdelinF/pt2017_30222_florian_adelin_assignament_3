package presentation;

/**
 * Created by Adelin on 23-Apr-17.
 */
        import java.awt.BorderLayout;

        import javax.swing.*;
        import javax.swing.border.EmptyBorder;
        import java.awt.Font;

public class AdminView extends JFrame {

    private JPanel contentPane;
    public JButton newClient = new JButton("New Client");
    public JButton newProduct = new JButton("New Product");
    public JButton deleteClient = new JButton("Delete Client");
    public JButton deleteProduct = new JButton("Delete Product");
    public JButton listProducts = new JButton("List Products");
    public JButton listClients = new JButton("List Clients");
    public JButton btnListOrders = new JButton("List Orders");
    public JButton btnMakeBill = new JButton("Make Bill");
    public JButton btnBack = new JButton("<=Back");
    JTextField billField = new JTextField();

    public AdminView() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.CENTER);
        panel.setLayout(null);

        newClient.setBounds(10, 95, 129, 23);
        panel.add(newClient);

        newProduct.setBounds(10, 129, 129, 23);
        panel.add(newProduct);

        deleteClient.setBounds(10, 163, 129, 23);
        panel.add(deleteClient);

        deleteProduct.setBounds(10, 197, 129, 23);
        panel.add(deleteProduct);

        listProducts.setBounds(149, 163, 119, 23);
        panel.add(listProducts);

        listClients.setBounds(149, 95, 119, 23);
        panel.add(listClients);

        btnListOrders.setBounds(149, 129, 119, 23);
        panel.add(btnListOrders);

        JLabel lblAdminPanelControl = new JLabel("Admin Panel Control");
        lblAdminPanelControl.setFont(new Font("Tahoma", Font.PLAIN, 40));
        lblAdminPanelControl.setBounds(20, 23, 404, 43);
        panel.add(lblAdminPanelControl);

        btnBack.setBounds(149, 197, 118, 23);
        panel.add(btnBack);

        btnMakeBill.setBounds(300, 197, 89, 23);
        panel.add(btnMakeBill);

        JLabel lblBillForClient = new JLabel("Bill for client with id:");
        lblBillForClient.setBounds(288, 138, 126, 14);
        panel.add(lblBillForClient);

        billField = new JTextField();
        billField.setBounds(303, 164, 86, 20);
        panel.add(billField);
        billField.setColumns(10);
    }

}
