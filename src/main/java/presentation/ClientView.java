package presentation;

/**
 * Created by Adelin on 23-Apr-17.
 */
        import java.awt.BorderLayout;
        import java.awt.EventQueue;

        import javax.swing.JFrame;
        import javax.swing.JPanel;
        import javax.swing.border.EmptyBorder;
        import javax.swing.JLabel;
        import java.awt.Font;
        import javax.swing.JButton;
        import javax.swing.JTextField;

public class ClientView extends JFrame {

    private JPanel contentPane;
    public JTextField productId;
    public JTextField quantityProduct;
    public JButton btnOrder = new JButton("Order");
    public JButton btnListProducts = new JButton("List Products");

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ClientView frame = new ClientView();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public ClientView() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.CENTER);
        panel.setLayout(null);

        JLabel lblClientPanelControl = new JLabel("Client Panel Control");
        lblClientPanelControl.setFont(new Font("Tahoma", Font.PLAIN, 40));
        lblClientPanelControl.setBounds(10, 11, 404, 40);
        panel.add(lblClientPanelControl);

        btnListProducts.setBounds(38, 74, 107, 167);
        panel.add(btnListProducts);

        btnOrder.setBounds(239, 191, 89, 23);
        panel.add(btnOrder);

        JLabel lblProductId = new JLabel("Product id");
        lblProductId.setBounds(239, 78, 146, 14);
        panel.add(lblProductId);

        productId = new JTextField();
        productId.setBounds(239, 103, 86, 20);
        panel.add(productId);
        productId.setColumns(10);

        JLabel lblQuantity = new JLabel("Quantity");
        lblQuantity.setBounds(239, 134, 46, 14);
        panel.add(lblQuantity);

        quantityProduct = new JTextField();
        quantityProduct.setBounds(239, 160, 86, 20);
        panel.add(quantityProduct);
        quantityProduct.setColumns(10);
    }

}

