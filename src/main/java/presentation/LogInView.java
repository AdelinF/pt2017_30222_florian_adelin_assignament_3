package presentation;

/**
 * Created by Adelin on 23-Apr-17.
 */
        import java.awt.BorderLayout;
        import java.awt.EventQueue;

        import javax.swing.JFrame;
        import javax.swing.JPanel;
        import javax.swing.border.EmptyBorder;
        import javax.swing.JLabel;
        import javax.swing.JTextField;
        import javax.swing.JButton;

public class LogInView extends JFrame {

    private JPanel contentPane;
    public JTextField userField = new JTextField();
    public JTextField passField = new JTextField();
    public JButton logInBtn = new JButton("Log In");


    public LogInView() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 200, 200);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.CENTER);
        panel.setLayout(null);

        JLabel lblUser = new JLabel("User");
        lblUser.setBounds(10, 11, 52, 14);
        panel.add(lblUser);

        JLabel lblPass = new JLabel("Pass");
        lblPass.setBounds(10, 36, 46, 14);
        panel.add(lblPass);

        userField.setBounds(66, 8, 86, 20);
        panel.add(userField);
        userField.setColumns(10);

        passField.setText("");
        passField.setBounds(66, 33, 86, 20);
        panel.add(passField);
        passField.setColumns(10);

        logInBtn.setBounds(10, 61, 154, 80);
        panel.add(logInBtn);
    }
}

