package presentation;

/**
 * Created by Adelin on 23-Apr-17.
 */
        import javax.swing.JFrame;
        import javax.swing.JPanel;
        import javax.swing.border.EmptyBorder;
        import javax.swing.JInternalFrame;
        import javax.swing.JLabel;
        import javax.swing.JTextField;
        import javax.swing.JButton;

public class DataInputView extends JFrame {

    private JPanel contentPane;
    public JTextField nameClient;
    public JTextField addressClient;
    public JTextField phoneClient;
    public JTextField passClient;
    public JTextField nameProduct;
    public JTextField priceProduct;
    public JTextField quantityProduct;
    public JTextField deleteId;
    public JTextField deleteIdProduct;
    public JButton btnDeleteProduct = new JButton("Delete");
    public JButton clientOkbtn = new JButton("OK");
    public JButton productOkbtn = new JButton("OK");
    public JButton btnDeleteClient = new JButton("Delete");


    public DataInputView() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 528, 365);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JInternalFrame clientInternalFrame = new JInternalFrame("New Client");
        clientInternalFrame.setBounds(0, 0, 156, 325);
        contentPane.add(clientInternalFrame);
        clientInternalFrame.getContentPane().setLayout(null);

        JLabel lblNume = new JLabel("Name");
        lblNume.setBounds(10, 6, 86, 14);
        clientInternalFrame.getContentPane().add(lblNume);

        nameClient = new JTextField();
        nameClient.setBounds(10, 31, 86, 20);
        clientInternalFrame.getContentPane().add(nameClient);
        nameClient.setColumns(10);

        JLabel lblAddress = new JLabel("Address");
        lblAddress.setBounds(10, 62, 86, 14);
        clientInternalFrame.getContentPane().add(lblAddress);

        addressClient = new JTextField();
        addressClient.setBounds(10, 87, 86, 20);
        clientInternalFrame.getContentPane().add(addressClient);
        addressClient.setColumns(10);

        JLabel lblPhone = new JLabel("Phone");
        lblPhone.setBounds(10, 118, 86, 14);
        clientInternalFrame.getContentPane().add(lblPhone);

        phoneClient = new JTextField();
        phoneClient.setBounds(10, 143, 86, 20);
        clientInternalFrame.getContentPane().add(phoneClient);
        phoneClient.setColumns(10);

        JLabel lblPassword = new JLabel("Password");
        lblPassword.setBounds(10, 167, 86, 14);
        clientInternalFrame.getContentPane().add(lblPassword);

        passClient = new JTextField();
        passClient.setBounds(10, 191, 86, 20);
        clientInternalFrame.getContentPane().add(passClient);
        passClient.setColumns(10);


        clientOkbtn.setBounds(10, 236, 89, 23);
        clientInternalFrame.getContentPane().add(clientOkbtn);

        JInternalFrame productInternalFrame = new JInternalFrame("New Product");
        productInternalFrame.setBounds(165, 0, 156, 325);
        contentPane.add(productInternalFrame);
        productInternalFrame.getContentPane().setLayout(null);

        JLabel lblName = new JLabel("Name");
        lblName.setBounds(10, 11, 86, 14);
        productInternalFrame.getContentPane().add(lblName);

        nameProduct = new JTextField();
        nameProduct.setBounds(10, 29, 86, 20);
        productInternalFrame.getContentPane().add(nameProduct);
        nameProduct.setColumns(10);

        JLabel lblPrice = new JLabel("Price");
        lblPrice.setBounds(10, 60, 86, 14);
        productInternalFrame.getContentPane().add(lblPrice);

        priceProduct = new JTextField();
        priceProduct.setBounds(10, 85, 86, 20);
        productInternalFrame.getContentPane().add(priceProduct);
        priceProduct.setColumns(10);

        JLabel lblQuantity = new JLabel("Quantity");
        lblQuantity.setBounds(10, 127, 86, 14);
        productInternalFrame.getContentPane().add(lblQuantity);

        quantityProduct = new JTextField();
        quantityProduct.setBounds(10, 152, 86, 20);
        productInternalFrame.getContentPane().add(quantityProduct);
        quantityProduct.setColumns(10);

        productOkbtn.setBounds(10, 237, 89, 23);
        productInternalFrame.getContentPane().add(productOkbtn);

        JInternalFrame deleteClient = new JInternalFrame("Delete Client");
        deleteClient.setBounds(331, 0, 181, 156);
        contentPane.add(deleteClient);
        deleteClient.getContentPane().setLayout(null);

        JLabel lblId = new JLabel("ID");
        lblId.setBounds(73, 11, 46, 14);
        deleteClient.getContentPane().add(lblId);

        deleteId = new JTextField();
        deleteId.setBounds(42, 27, 86, 20);
        deleteClient.getContentPane().add(deleteId);
        deleteId.setColumns(10);

        btnDeleteClient.setBounds(42, 62, 89, 23);
        deleteClient.getContentPane().add(btnDeleteClient);

        JInternalFrame internalFrame = new JInternalFrame("Delete Product");
        internalFrame.setBounds(331, 167, 181, 158);
        contentPane.add(internalFrame);
        internalFrame.getContentPane().setLayout(null);

        JLabel lblId_1 = new JLabel("ID");
        lblId_1.setBounds(73, 11, 46, 14);
        internalFrame.getContentPane().add(lblId_1);

        deleteIdProduct = new JTextField();
        deleteIdProduct.setBounds(48, 29, 86, 20);
        internalFrame.getContentPane().add(deleteIdProduct);
        deleteIdProduct.setColumns(10);

        btnDeleteProduct.setBounds(48, 60, 89, 23);
        internalFrame.getContentPane().add(btnDeleteProduct);
        internalFrame.setVisible(true);
        deleteClient.setVisible(true);
        productInternalFrame.setVisible(true);
        clientInternalFrame.setVisible(true);
    }
}

