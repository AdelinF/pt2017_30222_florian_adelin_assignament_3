package dao;

import connection.ConnectionFactory;
import model.Product;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Adelin on 26-Apr-17.
 */
public class ProductDAO {
    protected static final Logger LOGGER = Logger.getLogger(ProductDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO Product (Name,Price,Quantity)"
            + " VALUES (?,?,?)";
    private final static String findStatementString = "SELECT * FROM Product where idProduct = ?";
    private final static String deleteStatementString = "DELETE FROM Product where idProduct = ?";
    private final static String returnStatementString = "SELECT * FROM Product";
    private final static String updateStatementString = "UPDATE Product SET Quantity = ? WHERE idProduct = ?";

    public static Product findById(int productId) {
        Product toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, productId);
            rs = findStatement.executeQuery();
            rs.next();

            String name = rs.getString("Name");
            float price = rs.getFloat("Price");
            int  quantity = rs.getInt("Quantity");
            toReturn = new Product(productId, name,price, quantity);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static int insert(Product product) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, product.getName());
            insertStatement.setFloat(2, product.getPrice());
            insertStatement.setInt(3, product.getQuantity());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }

    public static int delete(int productId){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteStatementString);
            deleteStatement.setLong(1, productId);
            deleteStatement.executeUpdate();
        }catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:delete " + e.getMessage());
        } finally {
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }
        return 0;
    }

    public static ResultSet getResultSet(){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement returnStatement = null;
        ResultSet rs = null;
        try {
            returnStatement = dbConnection.prepareStatement(returnStatementString);
            rs = returnStatement.executeQuery();
        }catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:return " + e.getMessage());
        }
        return rs;
    }

    public static int update(int productId, int quantity){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement updateStatement = null;
        try {
            updateStatement = dbConnection.prepareStatement(updateStatementString);
            updateStatement.setLong(2, productId);
            updateStatement.setInt(1, quantity);
            updateStatement.executeUpdate();
        }catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:update " + e.getMessage());
        } finally {
            ConnectionFactory.close(updateStatement);
            ConnectionFactory.close(dbConnection);
        }
        return 0;
    }
}
