package dao;

import connection.ConnectionFactory;
import model.Order;

import javax.swing.*;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Adelin on 26-Apr-17.
 */
public class OrderDAO {
    protected static final Logger LOGGER = Logger.getLogger(OrderDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO `order` (idClient,idProduct,Quantity)"
            + " VALUES (?,?,?)";
    private final static String findStatementString = "SELECT * FROM `order` where idClient = ?";
    private final static String deleteStatementString = "DELETE FROM `order` where idOrder = ?";
    private final static String returnStatementString = "SELECT * FROM `order`";

    public static Order findById(int orderId) {
        Order toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, orderId);
            rs = findStatement.executeQuery();
            rs.next();

            int idC = rs.getInt("idClient");
            int idP = rs.getInt("idProduct");
            int  quantity = rs.getInt("Quantity");
            toReturn = new Order(idC, idP, quantity);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"OrderDAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static int insert(Order order) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setInt(1, order.getIdClient());
            insertStatement.setInt(2, order.getIdProduct());
            insertStatement.setInt(3, order.getQuantity());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,"Already bought this product");
            LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }
    public static int delete(int orderId){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteStatementString);
            deleteStatement.setLong(1, orderId);
            deleteStatement.executeUpdate();
        }catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrderDAO:delete " + e.getMessage());
        } finally {
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }
        return 0;
    }
    public static ResultSet getResultSet(){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement returnStatement = null;
        ResultSet rs = null;
        try {
            returnStatement = dbConnection.prepareStatement(returnStatementString);
            rs = returnStatement.executeQuery();
        }catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrderDAO:return " + e.getMessage());
        }
        return rs;
    }

}
