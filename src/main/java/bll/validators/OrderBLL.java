package bll.validators;

import bll.validators.validators.OrderValidator;
import bll.validators.validators.Validator;
import dao.OrderDAO;
import model.Order;

import javax.swing.*;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Adelin on 27-Apr-17.
 */
public class OrderBLL {
    private List<Validator<Order>> validators;

    public OrderBLL() {
        validators = new ArrayList<Validator<Order>>();
        validators.add(new OrderValidator());
    }

    public Order findOrderById(int id) {
        Order st = OrderDAO.findById(id);
        if (st == null) {
            JOptionPane.showMessageDialog(null,"Order was not found");
            throw new NoSuchElementException("The order with id =" + id + " was not found!");
        }
        return st;
    }

    public int insertOrder(Order order) {
        for (Validator<Order> v : validators) {
            v.validate(order);
        }
        return OrderDAO.insert(order);
    }

    public int deleteOrder(int id){
        return OrderDAO.delete(id);
    }

    public ResultSet listOrders(){
        return OrderDAO.getResultSet();
    }
}
