package bll.validators;

import bll.validators.validators.ProductValidator;
import bll.validators.validators.Validator;
import dao.ProductDAO;
import model.Product;

import javax.swing.*;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Adelin on 27-Apr-17.
 */
public class ProductBLL {
    private List<Validator<Product>> validators;

    public ProductBLL() {
        validators = new ArrayList<Validator<Product>>();
        validators.add(new ProductValidator());
    }

    public Product findProductById(int id) {
        Product st = ProductDAO.findById(id);
        if (st == null) {
            JOptionPane.showMessageDialog(null,"Product was not found");
            throw new NoSuchElementException("The product with id =" + id + " was not found!");
        }
        return st;
    }

    public int insertProduct(Product product) {
        for (Validator<Product> v : validators) {
            v.validate(product);
        }
        return ProductDAO.insert(product);
    }
    public int deleteProduct(int id){
        return ProductDAO.delete(id);
    }

    public int updateProduct(int id, int quantity){
        return ProductDAO.update(id,quantity);
    }

    public ResultSet listProducts(){
        return ProductDAO.getResultSet();
    }
}
