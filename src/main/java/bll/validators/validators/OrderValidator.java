package bll.validators.validators;

import model.Order;

import javax.swing.*;


/**
 * Created by Adelin on 27-Apr-17.
 */
public class OrderValidator implements Validator<Order>  {
    public void validate(Order t) {
        if (t.getQuantity()<1) {
            JOptionPane.showMessageDialog(null,"Order is not valid");
            throw new IllegalArgumentException("Order is not valid!");
        }
    }
}
