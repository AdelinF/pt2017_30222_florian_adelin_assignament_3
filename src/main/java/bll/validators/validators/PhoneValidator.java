package bll.validators.validators;

import model.Client;

import javax.swing.*;
import java.util.regex.Pattern;

/**
 * Created by Adelin on 27-Apr-17.
 */
public class PhoneValidator implements Validator<Client> {
    private static final String PHONE_PATTERN = "^(?=.{7,32}$)(\\(?\\+?[0-9]*\\)?)?[0-9_\\- \\(\\)]*((\\s?x\\s?|ext\\s?|extension\\s?)\\d{1,5}){0,1}$";
    public void validate(Client t) {
        Pattern pattern = Pattern.compile(PHONE_PATTERN);
        if (!pattern.matcher(t.getPhone()).matches()) {
            JOptionPane.showMessageDialog(null,"Phone is not valid");
            throw new IllegalArgumentException("Phone is not valid!");
        }
    }
}
