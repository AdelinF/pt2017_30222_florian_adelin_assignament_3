package bll.validators.validators;

import model.Product;

import javax.swing.*;

/**
 * Created by Adelin on 27-Apr-17.
 */
public class ProductValidator implements Validator<Product>  {
    public void validate(Product t) {
        if (t.getQuantity()<1) {
            JOptionPane.showMessageDialog(null,"Product quantity is not valid");
            throw new IllegalArgumentException("Product quantity is not valid!");
        }
    }
}
