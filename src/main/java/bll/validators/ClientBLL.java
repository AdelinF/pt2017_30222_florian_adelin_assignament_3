package bll.validators;

import bll.validators.validators.PhoneValidator;
import bll.validators.validators.Validator;
import dao.ClientDAO;
import model.Client;


import javax.swing.*;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Adelin on 26-Apr-17.
 */
public class ClientBLL {
    private List<Validator<Client>> validators;

    public ClientBLL() {
        validators = new ArrayList<Validator<Client>>();
        validators.add(new PhoneValidator());
    }

    public Client findClientById(int id) {
        Client st = ClientDAO.findById(id);
        if (st == null) {
            JOptionPane.showMessageDialog(null,"Client was not found");
            throw new NoSuchElementException("The client with id =" + id + " was not found!");
        }
        return st;
    }

    public int insertClient(Client student) {
        for (Validator<Client> v : validators) {
            v.validate(student);
        }
        return ClientDAO.insert(student);
    }

    public int deleteClient(int id){
        return ClientDAO.delete(id);
    }

    public ResultSet listClients(){
        return ClientDAO.getResultSet();
    }
}
